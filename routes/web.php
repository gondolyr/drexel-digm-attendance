<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::resource('courses', 'CourseController');

    Route::get('/', 'HomeController@index')->name('home');

    // Administrator
    Route::prefix('admin')->middleware(['admin'])->group(function () {
        Route::prefix('users')->group(function () {
            Route::get('/', 'UserController@index')->name('users.index');
            Route::get('create', 'UserController@create')->name('users.create');
            Route::post('/', 'UserController@store')->name('users.store');
            Route::get('{user}', 'UserController@show')->name('users.show');
            Route::get('{user}/edit', 'UserController@edit')->name('users.edit');
            Route::patch('{user}', 'UserController@update')->name('users.update');
            Route::post('{user}/reactivate', 'UserController@reactivate')->name('users.reactivate');
            Route::delete('{user}/deactivate', 'UserController@deactivate')->name('users.deactivate');
        });
    });

    // Attendance
    Route::prefix('courses')->group(function () {
        Route::get('{course}/attendances/take', 'AttendanceController@create')->name('attendances.create');
        Route::put('{course}/attendances/edit', 'AttendanceController@saveToday')->name('attendances.save');
        Route::get('{course}/students/{student}/attendances/edit', 'AttendanceController@edit')->name('attendances.students.edit');
        Route::patch('{course}/students/{student}/attendances', 'AttendanceController@update')->name('attendances.students.update');
    });

    // Logged-in user profile
    Route::prefix('profiles/me')->group(function () {
        Route::get('edit', 'ProfileController@edit')->name('profiles.edit');
        Route::patch('/', 'ProfileController@update')->name('profiles.update');
    });

    Route::prefix('students')->group(function () {
        Route::get('/', 'StudentController@index')->name('students.index');
        Route::get('create', 'StudentController@create')->name('students.create');
        Route::post('/', 'StudentController@store')->name('students.store');
        // No students.show route.
        Route::get('{student}/edit', 'StudentController@edit')->name('students.edit');
        Route::patch('{student}', 'StudentController@update')->name('students.update');
        Route::post('{student}/reactivate', 'StudentController@reactivate')->name('students.reactivate');
        Route::delete('{student}/deactivate', 'StudentController@deactivate')->name('students.deactivate');
    });
});
