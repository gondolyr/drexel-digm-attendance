<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;

/**
 * Class ProfileController
 *
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{
    /**
     * Show the edit form to update the logged-in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = auth()->user();

        return view('users.profile', compact('user'));
    }

    /**
     * Update the logged-in user's profile.
     *
     * @param ProfileRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileRequest $request)
    {
        $user = auth()->user();

        $user->first_name = request('first_name');
        $user->last_name = request('last_name');
        $user->email = request('email');
        $user->username = request('username');
        $user->updated_by = $user->id;

        // Only update the password if it is re-entered.
        if ($request->filled('password')) {
            $user->password = bcrypt(request('password'));
        }

        if ($user->save()) {
            session()->flash('success', 'Profile has been successfully updated');
        } else {
            session()->flash('error', 'Unable to update profile');
        }

        return redirect(route('profiles.edit'));
    }
}
