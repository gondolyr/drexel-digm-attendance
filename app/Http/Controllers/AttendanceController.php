<?php

namespace App\Http\Controllers;

use App\AttendanceStatus;
use App\Course;
use App\CourseAttendance;
use App\Http\Requests\AttendanceRequest;
use App\Student;
use Illuminate\Http\Request;

/**
 * Class AttendanceController
 *
 * @package App\Http\Controllers
 */
class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $course = Course::with(['students' => function ($query) use ($id) {
                            $query->with(['courseAttendances' => function ($query) use ($id) {
                                $query->where('course_id', $id)->today();
                            }]);
                        }])
                        ->find($id);

        if (is_null($course)) {
            session()->flash('error', 'Class could not be found');

            return back();
        } elseif ($course->students->isEmpty()) {
            session()->flash('warning', 'There are no students in this class');

            return back();
        }

        $attendanceStatuses = AttendanceStatus::orderBy('display_order')->get();

        return view('attendances.manual', compact('attendanceStatuses', 'course'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource or update an existing resource in storage for today.
     *
     * @param  AttendanceRequest  $request
     * @param  int                $id
     *
     * @return \Illuminate\Http\Response
     */
    public function saveToday(AttendanceRequest $request, $id)
    {
        $course = Course::with('students')->find($id);

        if (is_null($course)) {
            session()->flash('error', 'Class could not be found');

            return back();
        }

        $user = auth()->user();

        $success = true;
        $attendanceInputs = request('attendances');
        foreach ($attendanceInputs as $studentId => $studentInput) {
            if (!$course->students->contains('id', $studentId)) {
                // Student is not part of the class, skip
                continue;
            }

            $statusId = $studentInput['status'];
            $excused = isset($studentInput['excused']);

            $courseAttendance = CourseAttendance::today()
                ->firstOrNew(['course_id' => $course->id, 'student_id' => $studentId]);

            $courseAttendance->attendance_status_id = $statusId;
            $courseAttendance->excused = $excused;
            if (isset($studentInput['note'])) {
                $courseAttendance->note = $studentInput['note'];
            }
            $courseAttendance->updated_by = $user->id;

            if (!$courseAttendance->exists) {
                $courseAttendance->created_by = $user->id;
            }

            if (!$courseAttendance->save()) {
                $success = false;

                $student = Student::find($studentId);
                session()->flash('error', 'Attendance record for student: ' . $student->full_name);
            }
        }

        if ($success) {
            session()->flash('success', 'Attendance for course "' . $course->course_code . '" successfully saved');

            return redirect(route('courses.index'));
        } else {
            return redirect(route('attendances.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $courseId
     * @param  int  $studentId
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($courseId, $studentId)
    {
        $course = Course::with(['students' => function ($query) use ($courseId) {
                            $query->with(['courseAttendances' => function ($query) use ($courseId) {
                                $query->where('course_id', $courseId)->orderBy('created_at');
                            }]);
                        }])
                        ->whereHas('instructors', function ($query) {
                            $query->where('user_id', auth()->user()->id);
                        })
                        ->find($courseId);

        if (is_null($course)) {
            session()->flash('error', 'Class cannot be accessed');

            return redirect(back());
        } elseif ($course->students->isEmpty() || !$course->students->contains('id', $studentId)) {
            session()->flash('error', 'Student is not part of the class');

            return redirect(back());
        }

        $student = $course->students->where('id', $studentId)->first();
        $attendanceStatuses = AttendanceStatus::orderBy('display_order')->get();

        return view('attendances.student_list', compact('attendanceStatuses', 'course', 'student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AttendanceRequest $request
     * @param  int               $courseId
     * @param  int               $studentId
     *
     * @return \Illuminate\Http\Response
     */
    public function update(AttendanceRequest $request, $courseId, $studentId)
    {
        $course = Course::with(['students' => function ($query) use ($courseId) {
                            $query->with(['courseAttendances' => function ($query) use ($courseId) {
                                $query->where('course_id', $courseId);
                            }]);
                        }])
                        ->whereHas('instructors', function ($query) {
                            $query->where('user_id', auth()->user()->id);
                        })
                        ->find($courseId);

        if (is_null($course)) {
            session()->flash('error', 'Class cannot be accessed');

            return back();
        } elseif ($course->students->isEmpty() || !$course->students->contains('id', $studentId)) {
            session()->flash('error', 'Student is not part of the class');

            return redirect(back());
        }

        $user = auth()->user();
        $student = $course->students->where('id', $studentId)->first();

        $success = true;
        $attendanceInputs = request('attendances');
        foreach ($attendanceInputs as $attendanceId => $attendanceInput) {
            $statusId = $attendanceInput['status'];
            $excused = isset($attendanceInput['excused']);

            $courseAttendance = CourseAttendance::find($attendanceId);
            if (is_null($courseAttendance)) {
                // Invalid CourseAttendance ID but don't stop processing.
                session()->flash('error', 'Course Attendance ' . $attendanceId . ' is not a valid course attendance record');

                continue;
            }

            $courseAttendance->attendance_status_id = $statusId;
            $courseAttendance->excused = $excused;
            $courseAttendance->note = isset($attendanceInput['note']) ? $attendanceInput['note'] : null;
            $courseAttendance->updated_by = $user->id;

            if (!$courseAttendance->exists) {
                $courseAttendance->created_by = $user->id;
            }

            if (!$courseAttendance->save()) {
                $success = false;

                $student = Student::find($studentId);
                session()->flash('error', 'Attendance record for ' . $courseAttendance->created_at_date_time_12 . ' failed to update');
            }
        }

        if ($success) {
            session()->flash('success', 'Attendance for ' . $student->full_name . ' (' . $student->email . ') successfully saved');

            return redirect(route('courses.show', ['course' => $courseId]));
        } else {
            return redirect(route('attendances.create'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
