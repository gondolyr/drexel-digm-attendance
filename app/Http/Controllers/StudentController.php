<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Student;

/**
 * Class StudentController
 *
 * @package App\Http\Controllers
 */
class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::orderBy('active', 'desc')
            ->orderBy('last_name')
            ->orderBy('first_name')
            ->get();

        return view('students.list', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StudentRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        $studentData = [
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'university_id' => request('university_id'),
            'email' => request('email'),
        ];

        if (Student::create($studentData)) {
            session()->flash('success', 'Student has been successfully added');

            return redirect(route('students.index'));
        } else {
            session()->flash('error', 'Unable to add student');

            return redirect(route('students.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            session()->flash('error', 'Student could not be found');

            return back();
        }

        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StudentRequest  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, $id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            session()->flash('error', 'Student could not be found');

            return back();
        }

        $student->first_name = request('first_name');
        $student->last_name = request('last_name');
        $student->university_id = request('university_id');
        $student->email = request('email');

        if ($student->save()) {
            session()->flash('success', 'Student has been successfully updated');

            return redirect(route('students.index'));
        } else {
            session()->flash('error', 'Unable to update student');

            return redirect(route('students.edit', ['student' => $student->id]));
        }
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function deactivate($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (is_null($student)) {
            session()->flash('error', 'Student could not be found');

            return redirect(route('students.index'));
        }

        if ($student->deactivate()) {
            session()->flash('success', 'Student has been successfully deactivated');
        } else {
            session()->flash('error', 'Unable to deactivate student');
        }

        return redirect(route('students.index'));
    }

    /**
     * Reactivate the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function reactivate($id)
    {
        /** @var Student $student */
        $student = Student::find($id);

        if (is_null($student)) {
            session()->flash('error', 'Student could not be found');

            return redirect(route('students.index'));
        }

        if ($student->reactivate()) {
            session()->flash('success', 'Student has been successfully reactivated');
        } else {
            session()->flash('error', 'Unable to reactivate student');
        }

        return redirect(route('students.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);

        if (is_null($student)) {
            session()->flash('error', 'Student could not be found');

            return redirect(route('students.index'));
        }

        if ($student->delete()) {
            session()->flash('success', 'Student has been successfully removed');
        } else {
            session()->flash('error', 'Unable to remove student');
        }

        return redirect(route('students.index'));
    }
}
