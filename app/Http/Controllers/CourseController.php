<?php

namespace App\Http\Controllers;

use App\AttendanceStatus;
use App\Course;
use App\CourseTerm;
use App\Http\Requests\CourseRequest;
use App\Student;
use App\User;

/**
 * Class CourseController
 *
 * @package App\Http\Controllers
 */
class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::notExpired()
            ->with('students')
            ->whereHas('instructors', function ($query) {
                $query->where('user_id', auth()->user()->id);
            })
            ->orderBy('start_time')
            ->orderBy('start_date')
            ->get();

        $expiredCourses = Course::expired()
            ->with('students')
            ->whereHas('instructors', function ($query) {
                $query->where('user_id', auth()->user()->id);
            })
            ->orderBy('start_time')
            ->orderBy('start_date')
            ->get();

        return view('home', compact('courses', 'expiredCourses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courseTerms = CourseTerm::orderBy('display_order', 'asc')->get();
        $instructors = User::notSelf()->active()->orderBy('last_name')->orderBy('first_name')->get();
        $students = Student::active()->orderBy('last_name')->orderBy('first_name')->get();

        return view('courses.edit', compact(
            'courseTerms',
            'instructors',
            'students'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CourseRequest  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request)
    {
        $user = auth()->user();

        $courseData = [
            'crn' => request('crn'),
            'subject_code' => request('subject_code'),
            'course_number' => request('course_number'),
            'section' => request('section'),
            'title' => request('title'),
            'start_date' => request('start_date'),
            'end_date' => request('end_date'),
            'start_time' => request('start_time'),
            'end_time' => request('end_time'),
            'timezone' => request('timezone', config('app.timezone', 'America/New_York')),
            'monday' => request('monday', false),
            'tuesday' => request('tuesday', false),
            'wednesday' => request('wednesday', false),
            'thursday' => request('thursday', false),
            'friday' => request('friday', false),
            'course_term_id' => request('course_term_id'),
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ];

        $course = Course::create($courseData);

        if ($course) {
            session()->flash('success', 'Class has been successfully added');

            if ($user->is_admin) {
                $instructors = request('instructors');
                if (empty($instructors)) {
                    $instructors = [$user->id];
                }

                $course->instructors()->sync($instructors);
            }

            $course->students()->sync(request('students'));

            return redirect(route('courses.index'));
        } else {
            session()->flash('error', 'Unable to add class');

            return redirect(route('courses.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::with(['students' => function ($query) use ($id) {
            $query->active()
                ->with(['courseAttendances' => function ($query) use ($id) {
                    $query->where('course_id', $id);
                }]);
        }])
            ->with(['instructors' => function ($query) {
                $query->active();
            }])
            ->find($id);

        if (is_null($course)) {
            session()->flash('error', 'Class could not be found');

            return back();
        }

        // This only make a single database query and then we can filter on the returned collection.
        $attendanceStatuses = AttendanceStatus::whereIn('name', ['Present', 'Tardy', 'Absent'])->get();
        $absentStatusId = $attendanceStatuses->where('name', 'Absent')->first()->id;
        $tardyStatusId = $attendanceStatuses->where('name', 'Tardy')->first()->id;
        $presentStatusId = $attendanceStatuses->where('name', 'Present')->first()->id;

        return view('courses.view', compact('course', 'absentStatusId', 'tardyStatusId', 'presentStatusId'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::find($id);
        if (is_null($course)) {
            session()->flash('error', 'Class could not be found');

            return back();
        }

        $courseTerms = CourseTerm::orderBy('display_order', 'asc')->get();
        $instructors = User::notSelf()->active()->orderBy('last_name')->orderBy('first_name')->with('courses')->get();
        $students = Student::active()->orderBy('last_name')->orderBy('first_name')->with('courses')->get();

        return view('courses.edit', compact(
            'course',
            'courseTerms',
            'instructors',
            'students'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CourseRequest  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request, $id)
    {
        $course = Course::find($id);

        if (is_null($course)) {
            session()->flash('error', 'Class could not be found');

            return back();
        }

        $user = auth()->user();

        $course->crn = request('crn');
        $course->subject_code = request('subject_code');
        $course->course_number = request('course_number');
        $course->section = request('section');
        $course->title = request('title');
        $course->start_date = request('start_date');
        $course->end_date = request('end_date');
        $course->start_time = request('start_time');
        $course->end_time = request('end_time');
        $course->timezone = request('timezone', config('app.timezone', 'America/New_York'));
        $course->monday = request('monday', false);
        $course->tuesday = request('tuesday', false);
        $course->wednesday = request('wednesday', false);
        $course->thursday = request('thursday', false);
        $course->friday = request('friday', false);
        $course->course_term_id = request('course_term_id');
        $course->updated_by = $user->id;

        if ($course->save()) {
            session()->flash('success', 'Class has been successfully updated');

            if ($user->is_admin) {
                $instructors = request('instructors');
                if (empty($instructors)) {
                    $instructors = [$user->id];
                }

                $course->instructors()->sync($instructors);
            }

            $course->students()->sync(request('students'));

            return redirect(route('courses.index'));
        } else {
            session()->flash('error', 'Unable to update class');

            return redirect(route('courses.edit', ['course' => $course->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
