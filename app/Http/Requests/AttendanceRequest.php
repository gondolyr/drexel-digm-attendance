<?php

namespace App\Http\Requests;

use App\Course;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class AttendanceRequest
 *
 * @package App\Http\Requests
 */
class AttendanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $course = Course::with('instructors')
                        ->find($this->route('course'));

        return auth()->check() &&
            $course &&
            $course->instructors->isNotEmpty() &&
            $course->instructors->contains('id', auth()->user()->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'attendances.*.status' => 'required|exists:attendance_statuses,id',
            'attendances.*.excused' => 'nullable|boolean',
            'attendances.*.note' => 'nullable|string|max:65535',
        ];
    }
}
