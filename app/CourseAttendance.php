<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CourseAttendance
 * @package App
 */
class CourseAttendance extends Model
{
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'excused' => 'boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_id',
        'student_id',
        'attendance_status_id',
        'created_by',
        'updated_by',
    ];

    /**
     * Get the attendance status associated with the course attendance record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attendanceStatus()
    {
        return $this->belongsTo(AttendanceStatus::class);
    }

    /**
     * Get the course associated with the course attendance record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    /**
     * Get the student associated with this course attendance record.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(Student::class);
    }

    /**
     * Get the attendance records created today.
     * Query scope for chaining.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToday($query)
    {
        return $query->whereDate('created_at', Carbon::today());
    }

    /**
     * Get the attendance records created on a specific day.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  string                                $date  Date to get records for.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDate($query, $date)
    {
        // Normalize the $date parameter.
        $datetime = Carbon::parse($date);

        return $query->whereDate('created_at', $datetime->toDateString());
    }

    /**
     * Get the attendance records that are absent.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAbsences($query)
    {
        $absentStatus = AttendanceStatus::absent();

        return $query->where('attendance_status_id', $absentStatus->id);
    }

    /**
     * Get the attendance records that are tardy.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeTardies($query)
    {
        $tardyStatus = AttendanceStatus::tardy();

        return $query->where('attendance_status_id', $tardyStatus->id);
    }

    /**
     * Get the attendance records that are present.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePresent($query)
    {
        $presentStatus = AttendanceStatus::present();

        return $query->where('attendance_status_id', $presentStatus->id);
    }

    /**
     * Get the creation date in 12-hour format.
     *
     * @param  string|null $timezone
     *
     * @return string
     */
    public function getCreatedAtDateTime12($timezone = null)
    {
        if (is_null($timezone)) {
            $timezone = config('app.timezone');
        }

        return Carbon::parse($this->created_at)->timezone($timezone)->format('F d, o, h:i A');
    }

    /**
     * Get the creation date's day of the week as the full name.
     *
     * @param  string|null $timezone
     *
     * @return string
     */
    public function getCreatedAtDayOfWeek($timezone = null)
    {
        if (is_null($timezone)) {
            $timezone = config('app.timezone');
        }

        return Carbon::parse($this->created_at)->timezone($timezone)->format('l');
    }
}
