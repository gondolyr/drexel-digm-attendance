<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use PDOException;

/**
 * Class UserTest
 *
 * @package Tests\Unit
 */
class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that a user can be created and fetched properly.
     */
    public function testCreateUser()
    {
        $user = factory(User::class)->create();

        $savedUser = User::find($user->id);

        $this->assertEquals($user->first_name, $savedUser->first_name);
        $this->assertEquals($user->last_name, $savedUser->last_name);
        $this->assertEquals($user->email, $savedUser->email);
        $this->assertEquals($user->username, $savedUser->username);
        $this->assertEquals($user->active, $savedUser->active);
        $this->assertEquals($user->remember_token, $savedUser->remember_token);
        $this->assertEquals($user->created_by, $savedUser->created_by);
        $this->assertEquals($user->updated_by, $savedUser->updated_by);
    }

    /**
     * Attempt to save two users with the same email.
     */
    public function testCreateTwoUsersWithSameEmailResultsInError()
    {
        $user1 = factory(User::class)->create();
        $this->expectException(\PDOException::class);
        factory(User::class)->create([
            'email' => $user1->email,
        ]);
    }

    /**
     * Attempt to save two users with the same email but different casing.
     */
    public function testCreateTwoUsersWithSameEmailDifferentCasingResultsInError()
    {
        // TODO: Find out why this test doesn't raise an exception on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $user1 = factory(User::class)->create([
            'email' => 'test@example.com',
        ]);
        factory(User::class)->create([
            'email' => 'TEST@EXAMPLE.COM',
        ]);

        $this->expectException(\PDOException::class);
        $this->expectExceptionMessage("PDOException: SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '" . $user1->email . "' for key 'users_email_unique'");
    }

    /**
     * Attempt to create two users with similar-looking emails.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoUsersWithSimilarEmailButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER A"
        $userWithLatin = factory(User::class)->create([
            'email' => "j\u{0061}ne_doe@example.com",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $userWithCyrillic = factory(User::class)->create([
            'email' => "j\u{0430}ne_doe@example.com",
        ]);

        $savedUserWithLatin = User::find($userWithLatin->id);
        $savedUserWithCyrillic = User::find($userWithCyrillic->id);

        // Assert that the database saved the Unicode.
        $this->assertEquals($userWithLatin->email, $savedUserWithLatin->email);
        $this->assertEquals($userWithCyrillic->email, $savedUserWithCyrillic->email);

        // Assert that the ASCII 'a' does not match the Latin 'a'.
        $this->assertNotEquals('jane_doe@example.com', $savedUserWithCyrillic->email);
    }

    /**
     * Attempt to create a user with a username that already exists.
     */
    public function testCreateUserWithSameUsername()
    {
        $user1 = factory(User::class)->create();
        $this->expectException(\PDOException::class);
        factory(User::class)->create([
            'username' => $user1->username,
        ]);
    }

    /**
     * Attempt to create two users with the same username but different casing.
     */
    public function testCreateUserWithSameUsernameButDifferentCasing()
    {
        // TODO: Find out why this test fails on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $user1 = factory(User::class)->create([
            'username' => 'foobarbaz',
        ]);
        factory(User::class)->create([
            'username' => 'FOObarBAZ',
        ]);

        $this->expectException(\PDOException::class);
        $this->expectExceptionMessage("PDOException: SQLSTATE[23000]: Integrity constraint violation: 1062 Duplicate entry '" . $user1->username . "' for key 'users_username_unique'");
    }

    /**
     * Attempt to create two users with similar-looking usernames.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoUsersWithSimilarUsernameButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER"
        $userWithLatin = factory(User::class)->create([
            'username' => "j\u{0061}n_doe",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $userWithCyrillic = factory(User::class)->create([
            'username' => "j\u{0430}n_doe",
        ]);

        $savedUserWithLatin = User::find($userWithLatin->id);
        $savedUserWithCyrillic = User::find($userWithCyrillic->id);

        // Assert that the database saved the Unicode.
        $this->assertEquals($userWithLatin->username, $savedUserWithLatin->username);
        $this->assertEquals($userWithCyrillic->username, $savedUserWithCyrillic->username);

        // Assert that the ASCII 'a' does not match the Cyrillic 'a'.
        $this->assertNotEquals('jan_doe', $savedUserWithCyrillic->username);
    }

    /**
     * Attempt to update a user's username to one that already exists.
     */
    public function testUpdateUserUsernameToExistingEntry()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $user1->username = $user2->username;
        $this->expectException(\PDOException::class);
        $user1->save();
    }

    /**
     * Assert that the query scope only fetches the users that are not the authenticated user.
     */
    public function testGetUsersExceptAuthenticatedUser()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $user2 = factory(User::class)->create();
        $user3 = factory(User::class)->create();

        $users = User::notSelf()->whereIn('id', [$authenticatedUser->id, $user2->id, $user3->id])->get();

        $this->assertCount(2, $users);
    }

    /**
     * Assert that the model has a full_name attribute accessor
     * that formats the first and last name into a single string,
     * separated by a single space between them.
     */
    public function testGetFullNameAttribute()
    {
        $user = factory(User::class)->create();

        $expectedFullName = $user->first_name . ' ' . $user->last_name;
        $this->assertEquals($expectedFullName, $user->full_name);
    }

    /**
     * Assert that the is_active_readable attribute returns the string 'Yes' when user is active.
     */
    public function testGetIsActiveReadableAttributeAsActive()
    {
        $user = factory(User::class)->create();

        $this->assertEquals('Yes', $user->is_active_readable);
    }

    /**
     * Assert that the is_active_readable attribute returns the string 'No' when user is not active.
     */
    public function testGetIsActiveReadableAttributeAsInactive()
    {
        $user = factory(User::class)->create([
            'active' => false,
        ]);

        $this->assertEquals('No', $user->is_active_readable);
    }

    /**
     * Assert that the full_name_and_username attribute returns a string with the following format:
     *
     *      first_name last_name (username)
     *
     * Example:
     *      John Smith (john_smith)
     */
    public function testGetFullNameAndUsernameAttribute()
    {
        $user = factory(User::class)->create();

        $expected = $user->full_name . ' (' . $user->username . ')';
        $this->assertEquals($expected, $user->full_name_and_username);
    }
}
