<?php

namespace Tests\Unit;

use App\Role;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use PDOException;

/**
 * Class RoleTest
 *
 * @package Tests\Unit
 */
class RoleTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an role can be created and fetched properly.
     */
    public function testCreateRole()
    {
        $role = factory(Role::class)->create();

        $savedRole = Role::find($role->id);

        $this->assertEquals($role->name, $savedRole->name);
        $this->assertEquals($role->created_at, $savedRole->created_at);
        $this->assertEquals($role->updated_at, $savedRole->updated_at);
    }

    /**
     * Attempt to create two roles with the same name.
     */
    public function testCreateTwoRoleWithSameName()
    {
        $role1 = factory(Role::class)->create();
        $this->expectException(\PDOException::class);
        factory(Role::class)->create([
            'name' => $role1->name,
        ]);
    }

    /**
     * Attempt to save two roles with the same name but different casing.
     */
    public function testCreateTwoRolesWithSameEmailDifferentCasingResultsInError()
    {
        // TODO: Find out why a PDOException is not thrown with SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        $role1 = factory(Role::class)->create([
            'name' => 'Average Joe',
        ]);
        factory(Role::class)->create([
            'name' => 'AVERAGE JOE',
        ]);

        $this->expectException(\PDOException::class);
    }

    /**
     * Attempt to create two roles with similar-looking names.
     * One uses a Latin 'a' and the other uses a Cyrillic 'a'.
     */
    public function testCreateTwoRolesWithSimilarEmailButWithDifferentUnicodeCharacters()
    {
        // Has "U+0061 LATIN SMALL LETTER A"
        $roleWithLatin = factory(Role::class)->create([
            'name' => "St\u{0061}ndard John",
        ]);
        // Has "U+0430 CYRILLIC SMALL LETTER A"
        $roleWithCyrillic = factory(Role::class)->create([
            'name' => "St\u{0430}ndard John",
        ]);

        $savedRoleWithLatin = Role::find($roleWithLatin->id);
        $savedRoleWithCyrillic = Role::find($roleWithCyrillic->id);

        // Assert that the Unicode character was saved correctly.
        $this->assertEquals($roleWithLatin->name, $savedRoleWithLatin->name);
        $this->assertEquals($roleWithCyrillic->name, $savedRoleWithCyrillic->name);

        // Assert that the ASCII 'a' does not match the Cyrillic 'a'.
        $this->assertNotEquals('Standard John', $roleWithCyrillic->name);
    }
}
