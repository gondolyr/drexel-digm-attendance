<?php

namespace Tests\Unit;

use App\Course;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use PDOException;

/**
 * Class CourseTest
 *
 * @package Tests\Unit
 */
class CourseTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test that an course can be created and fetched properly.
     */
    public function testCreateCourse()
    {
        $course = factory(Course::class)->create();

        $savedCourse = Course::find($course->id);

        $this->assertEquals($course->crn, $savedCourse->crn);
        $this->assertEquals($course->subject_code, $savedCourse->subject_code);
        $this->assertEquals($course->course_number, $savedCourse->course_number);
        $this->assertEquals($course->section, $savedCourse->section);
        $this->assertEquals($course->title, $savedCourse->title);
        $this->assertEquals($course->start_date, $savedCourse->start_date);
        $this->assertEquals($course->end_date, $savedCourse->end_date);
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $savedCourse->start_time);
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $savedCourse->end_time);
        $this->assertEquals($course->course_term_id, $savedCourse->course_term_id);
        $this->assertEquals($course->monday, $savedCourse->monday);
        $this->assertEquals($course->tuesday, $savedCourse->tuesday);
        $this->assertEquals($course->wednesday, $savedCourse->wednesday);
        $this->assertEquals($course->thursday, $savedCourse->thursday);
        $this->assertEquals($course->friday, $savedCourse->friday);
        $this->assertEquals($course->timezone, $savedCourse->timezone);
        $this->assertEquals($course->created_at, $savedCourse->created_at);
        $this->assertEquals($course->updated_at, $savedCourse->updated_at);
    }

    /**
     * Attempt to create a course with a matching CRN.
     */
    public function testCreateTwoCourseWithSameCrn()
    {
        $course1 = factory(Course::class)->create();
        $this->expectException(\PDOException::class);
        factory(Course::class)->create([
            'crn' => $course1->crn,
        ]);
    }

    /**
     * Attempt to create a course with a course_term_id value that does not exist in the course_terms table.
     */
    public function testCreateCourseWithInvalidCourseTermIdNumber()
    {
        // TODO: Find out why a PDOException is not thrown on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        factory(Course::class)->create([
            'course_term_id' => 0,
        ]);
        $this->expectException(\PDOException::class);
    }

    /**
     * Attempt to create a course with an invalid start date.
     */
    public function testCreateCourseWithInvalidStartDate()
    {
        // TODO: Find out why a PDOException is not thrown on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        factory(Course::class)->create([
            'start_date' => '2018-13-40',
        ]);
        $this->expectException(\PDOException::class);
    }

    /**
     * Attempt to create a course with an invalid end date.
     */
    public function testCreateCourseWithInvalidEndDate()
    {
        // TODO: Find out why a PDOException is not thrown on SQLite
        $this->markTestSkipped("PDOException should be thrown but isn't");

        factory(Course::class)->create([
            'end_date' => '2018-02-30',
        ]);
        $this->expectException(\PDOException::class);
    }

    /**
     * Assert that the accessor returns a time string in 12-hour format.
     */
    public function testAccessorStartTime12Returns12HourFormat()
    {
        $course = factory(Course::class)->create();

        $this->assertTrue(Carbon::hasFormat($course->start_time_12, 'h:i A'));
        $this->assertEquals(Carbon::parse($course->start_time)->format('h:i A'), $course->start_time_12);
    }

    /**
     * Assert that the accessor returns a time string in 12-hour format.
     */
    public function testAccessorEndTime12Returns12HourFormat()
    {
        $course = factory(Course::class)->create();

        $this->assertTrue(Carbon::hasFormat($course->end_time_12, 'h:i A'));
        $this->assertEquals(Carbon::parse($course->end_time)->format('h:i A'), $course->end_time_12);
    }

    /**
     * Assert that the accessor returns a time string in 24-hour format.
     */
    public function testAccessorStartTime24Returns24HourFormat()
    {
        $course = factory(Course::class)->create();

        $this->assertTrue(Carbon::hasFormat($course->start_time_24, 'H:i'));
        $this->assertEquals(Carbon::parse($course->start_time)->format('H:i'), $course->start_time_24);
    }

    /**
     * Assert that the accessor returns a time string in 24-hour format.
     */
    public function testAccessorEndTime24Returns24HourFormat()
    {
        $course = factory(Course::class)->create();

        $this->assertTrue(Carbon::hasFormat($course->end_time_24, 'H:i'));
        $this->assertEquals(Carbon::parse($course->end_time)->format('H:i'), $course->end_time_24);
    }

    /**
     * Assert that the accessor returns true when the course's end date is older than today.
     */
    public function testAccessorExpiredReturnsTrueWithEndDateOlderThanToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::yesterday(),
        ]);

        $this->assertTrue($course->expired);
    }

    /**
     * Assert that the accessor returns false when the course's end date is today. Timezone is factored into determining if it is expired.
     */
    public function testAccessorExpiredReturnsFalseWithEndDateSameDateAsToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::today(),
        ]);

        $this->assertFalse($course->expired);
    }

    /**
     * Assert that the accessor returns false when the course's end date is later than today.
     */
    public function testAccessorExpiredReturnsTrueWithEndDateLaterThanToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::tomorrow(),
        ]);

        $this->assertFalse($course->expired);
    }

    /**
     * Assert that the accessor returns false when the course's end date is older than today.
     */
    public function testAccessorNotExpiredReturnsTrueWithEndDateOlderThanToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::yesterday(),
        ]);

        $this->assertFalse($course->not_expired);
    }

    /**
     * Assert that the accessor returns true when the course's end date is today. Timezone is factored into determining if it is expired.
     */
    public function testAccessorNotExpiredReturnsFalseWithEndDateSameDateAsToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::today(),
        ]);

        $this->assertTrue($course->not_expired);
    }

    /**
     * Assert that the accessor returns true when the course's end date is later than today.
     */
    public function testAccessorNotExpiredReturnsTrueWithEndDateLaterThanToday()
    {
        $course = factory(Course::class)->create([
            'end_date' => Carbon::tomorrow(),
        ]);

        $this->assertTrue($course->not_expired);
    }
}
