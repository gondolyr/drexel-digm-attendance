<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ProfileRouteTest
 * @package Tests\Feature
 */
class ProfileRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Assert that the page can be accessed if logged in.
     */
    public function testAccessEditPageAuthorized()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('profiles.edit'));
        $response->assertStatus(200);
    }

    /**
     * Update the logged in user's profile.
     *
     * Test that the submitted form is processed as valid.
     */
    public function testUpdateProfileWithValidData()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect(route('profiles.edit'));

        $fetchedUser = User::find($user->id);
        $this->assertEquals($newUserDetails->first_name, $fetchedUser->first_name);
        $this->assertEquals($newUserDetails->last_name, $fetchedUser->last_name);
        $this->assertEquals($newUserDetails->email, $fetchedUser->email);
        $this->assertEquals($newUserDetails->username, $fetchedUser->username);
        $this->assertTrue(auth()->validate([
            'username' => $user->username,
            'password' => 'testpassword',
        ]));
    }

    public function testUpdateProfileWithBlankPasswordDoesNotChangePassword()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        // First set the password to a value we know.
        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect(route('profiles.edit'));

        // Now send the blank password.
        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => '',
            'password_confirmation' => '',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect(route('profiles.edit'));

        $fetchedUser = User::find($user->id);
        $this->assertEquals($newUserDetails->first_name, $fetchedUser->first_name);
        $this->assertEquals($newUserDetails->last_name, $fetchedUser->last_name);
        $this->assertEquals($newUserDetails->email, $fetchedUser->email);
        $this->assertEquals($newUserDetails->username, $fetchedUser->username);
        $this->assertTrue(auth()->validate([
            'username' => $user->username,
            'password' => 'testpassword',
        ]));
    }

    /**
     * Test that the guest is redirected from the page when not logged in.
     */
    public function testAccessEditPageUnauthorized()
    {
        $response = $this->get(route('profiles.edit'));
        $response->assertRedirect();
    }

    /**
     * Test that a guest cannot update a profile and is instead redirected to the login page.
     */
    public function testUpdateProfileUnauthorized()
    {
        $user = factory(User::class)->create();

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect('login');

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their first name to an empty string.
     */
    public function testUpdateProfileWithBlankFirstNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => '',
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their last name to an empty string.
     */
    public function testUpdateProfileWithBlankLastNameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => '',
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
    }

    /**
     * Assert that a user cannot set their email to an empty string.
     */
    public function testUpdateProfileWithBlankEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => '',
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their email to an invalid email address.
     */
    public function testUpdateProfileWithInvalidEmailFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => 'notavalidemailaddress',
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their email to an existing email address.
     */
    public function testUpdateProfileWithExistingEmailRecordFailsValidation()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $this->actingAs($user1);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $user2->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user1->id);
        $this->assertEquals($user1->first_name, $fetchedUser->first_name);
        $this->assertEquals($user1->last_name, $fetchedUser->last_name);
        $this->assertEquals($user1->email, $fetchedUser->email);
        $this->assertEquals($user1->username, $fetchedUser->username);
        $this->assertEquals($user1->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their username to an empty string.
     */
    public function testUpdateProfileWithBlankUsernameFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => '',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their username to an existing username.
     */
    public function testUpdateProfileWithExistingUsernameRecordFailsValidation()
    {
        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $this->actingAs($user1);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $user2->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user1->id);
        $this->assertEquals($user1->first_name, $fetchedUser->first_name);
        $this->assertEquals($user1->last_name, $fetchedUser->last_name);
        $this->assertEquals($user1->email, $fetchedUser->email);
        $this->assertEquals($user1->username, $fetchedUser->username);
        $this->assertEquals($user1->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set their password that does not meet the minimum length (6).
     */
    public function testUpdateProfileWithShortPasswordFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'test',
            'password_confirmation' => 'test',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that the user's profile is not saved when the password and password confirmation do not match.
     */
    public function testUpdateProfileWithPasswordConfirmationNotEqualFailsValidation()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'test',
        ];

        $response = $this->patch(route('profiles.update'), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }
}
