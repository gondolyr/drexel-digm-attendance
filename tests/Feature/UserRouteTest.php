<?php

namespace Tests\Feature;

use App\Role;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

/**
 * Class AdminRouteTest
 *
 * @package Tests\Feature
 */
class UserRouteTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Assert that the users list page can be accessed if logged in.
     */
    public function testAccessUserListPageAuthorized()
    {
        $user = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $user->roles()->attach($adminRole);
        $this->actingAs($user);

        $response = $this->get(route('users.index'));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the user view page can be accessed if logged in.
     */
    public function testAccessUserViewPageAuthorized()
    {
        $user = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $user->roles()->attach($adminRole);
        $this->actingAs($user);

        $response = $this->get(route('users.show', ['user' => $user->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the user creation page can be accessed if logged in.
     */
    public function testAccessUserCreatePageAuthorized()
    {
        $user = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $user->roles()->attach($adminRole);
        $this->actingAs($user);

        $response = $this->get(route('users.create'));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the user update page can be accessed if logged in.
     */
    public function testAccessUserUpdatePageAuthorized()
    {
        $authenticatedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authenticatedUser->roles()->attach($adminRole);
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();

        $response = $this->get(route('users.edit', ['user' => $user->id]));
        $this->assertTrue($response->isOk());
    }

    /**
     * Assert that the user attempting to edit themselves through the admin edit screen
     * is redirected to the profile edit screen.
     */
    public function testAccessUserUpdatePageAsSelf()
    {
        $authenticatedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authenticatedUser->roles()->attach($adminRole);
        $this->actingAs($authenticatedUser);

        $response = $this->get(route('users.edit', ['user' => $authenticatedUser->id]));
        $response->assertRedirect(route('profiles.edit'));
    }

    /**
     * Assert that a non-admin user attempting to edit themselves through the admin edit screen
     * is redirected (to the previous page they were on).
     */
    public function testAccessUserUpdatePageAsNonAdminSelf()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $response = $this->get(route('users.edit', ['user' => $authenticatedUser->id]));
        $response->assertRedirect();
    }

    /**
     * Update the logged in user's profile.
     *
     * Test that the submitted form is processed as valid.
     */
    public function testUpdateProfileWithValidData()
    {
        $authenticatedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authenticatedUser->roles()->attach($adminRole);
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect(route('users.index'));

        $fetchedUser = User::find($user->id);
        $this->assertEquals($newUserDetails->first_name, $fetchedUser->first_name);
        $this->assertEquals($newUserDetails->last_name, $fetchedUser->last_name);
        $this->assertEquals($newUserDetails->email, $fetchedUser->email);
        $this->assertEquals($newUserDetails->username, $fetchedUser->username);
        $this->assertTrue(auth()->validate([
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
        ]));
    }

    /**
     * Assert that the user's password does not change when the password fields are left blank.
     */
    public function testUpdateProfileWithBlankPasswordDoesNotChangePassword()
    {
        $authenticatedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authenticatedUser->roles()->attach($adminRole);
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $adminUserUpdateRoute = route('users.update', ['user' => $user->id]);
        $adminIndexRoute = route('users.index');

        // First set the password to a value we know.
        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch($adminUserUpdateRoute, $data);
        $response->assertRedirect($adminIndexRoute);

        // Now send the blank password.
        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => '',
            'password_confirmation' => '',
        ];

        $response = $this->patch($adminUserUpdateRoute, $data);
        $response->assertRedirect($adminIndexRoute);

        $fetchedUser = User::find($user->id);
        $this->assertEquals($newUserDetails->first_name, $fetchedUser->first_name);
        $this->assertEquals($newUserDetails->last_name, $fetchedUser->last_name);
        $this->assertEquals($newUserDetails->email, $fetchedUser->email);
        $this->assertEquals($newUserDetails->username, $fetchedUser->username);
        $this->assertTrue(auth()->validate([
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
        ]));
    }

    /**
     * Test that the guest is redirected from the page when not logged in.
     */
    public function testAccessUserListPageUnauthorized()
    {
        $response = $this->get(route('users.index'));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not an admin.
     */
    public function testAccessUserListPageNotAsAdmin()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('users.index'));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not logged in.
     */
    public function testAccessUserViewPageUnauthorized()
    {
        $user = factory(User::class)->create();

        $response = $this->get(route('users.show', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not an admin.
     */
    public function testAccessUserViewPageNotAsAdmin()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('users.show', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not logged in.
     */
    public function testAccessUserCreatePageUnauthorized()
    {
        $response = $this->get(route('users.create'));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not an admin.
     */
    public function testAccessUserCreatePageNotAsAdmin()
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);

        $response = $this->get(route('users.create'));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not logged in.
     */
    public function testAccessUserEditPageUnauthorized()
    {
        $user = factory(User::class)->create();

        $response = $this->get(route('users.edit', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Test that the guest is redirected from the page when not an admin.
     */
    public function testAccessUserEditPageNotAsAdmin()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();

        $response = $this->get(route('users.edit', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Test that a guest cannot update a user and is instead redirected to the login page.
     */
    public function testUpdateUserUnauthorized()
    {
        $user = factory(User::class)->create();

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect(route('login'));

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Test that a guest cannot update a user and is instead redirected to the login page.
     */
    public function testUpdateUserNotAsAdmin()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's first name to an empty string.
     */
    public function testUpdateProfileWithBlankFirstNameFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => '',
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's last name to an empty string.
     */
    public function testUpdateProfileWithBlankLastNameFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => '',
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
    }

    /**
     * Assert that a user cannot set a user's email to an empty string.
     */
    public function testUpdateProfileWithBlankEmailFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => '',
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's email to an invalid email address.
     */
    public function testUpdateProfileWithInvalidEmailFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => 'notavalidemailaddress',
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's email to an existing email address.
     */
    public function testUpdateProfileWithExistingEmailRecordFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $user2->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user1->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user1->id);
        $this->assertEquals($user1->first_name, $fetchedUser->first_name);
        $this->assertEquals($user1->last_name, $fetchedUser->last_name);
        $this->assertEquals($user1->email, $fetchedUser->email);
        $this->assertEquals($user1->username, $fetchedUser->username);
        $this->assertEquals($user1->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's username to an empty string.
     */
    public function testUpdateProfileWithBlankUsernameFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => '',
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's username to an existing username.
     */
    public function testUpdateProfileWithExistingUsernameRecordFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user1 = factory(User::class)->create();
        $user2 = factory(User::class)->create();

        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $user2->username,
            'password' => 'testpassword',
            'password_confirmation' => 'testpassword',
        ];

        $response = $this->patch(route('users.update', ['user' => $user1->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user1->id);
        $this->assertEquals($user1->first_name, $fetchedUser->first_name);
        $this->assertEquals($user1->last_name, $fetchedUser->last_name);
        $this->assertEquals($user1->email, $fetchedUser->email);
        $this->assertEquals($user1->username, $fetchedUser->username);
        $this->assertEquals($user1->password, $fetchedUser->password);
    }

    /**
     * Assert that a user cannot set a user's password that does not meet the minimum length (6).
     */
    public function testUpdateProfileWithShortPasswordFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'test',
            'password_confirmation' => 'test',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that the user's details is not saved when the password and password confirmation do not match.
     */
    public function testUpdateProfileWithPasswordConfirmationNotEqualFailsValidation()
    {
        $authorizedUser = factory(User::class)->create();
        $adminRole = Role::admin()->first();
        $authorizedUser->roles()->attach($adminRole);
        $this->actingAs($authorizedUser);

        $user = factory(User::class)->create();
        $newUserDetails = factory(User::class)->make();

        $data = [
            'first_name' => $newUserDetails->first_name,
            'last_name' => $newUserDetails->last_name,
            'email' => $newUserDetails->email,
            'username' => $newUserDetails->username,
            'password' => 'testpassword',
            'password_confirmation' => 'test',
        ];

        $response = $this->patch(route('users.update', ['user' => $user->id]), $data);
        $response->assertRedirect();

        // Ensure that the update did not happen.
        $fetchedUser = User::find($user->id);
        $this->assertEquals($user->first_name, $fetchedUser->first_name);
        $this->assertEquals($user->last_name, $fetchedUser->last_name);
        $this->assertEquals($user->email, $fetchedUser->email);
        $this->assertEquals($user->username, $fetchedUser->username);
        $this->assertEquals($user->password, $fetchedUser->password);
    }

    /**
     * Assert that a user can be deactivated using the HTTP DELETE method.
     */
    public function testDeactivateUser()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create();

        $response = $this->delete(route('users.deactivate', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that the logged-in user cannot delete themselves.
     */
    public function testUserCannotDeactivateThemselves()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $response = $this->delete(route('users.deactivate', ['user' => $authenticatedUser->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that a user that has been deactivated cannot be deactivated again.
     */
    public function testDeleteUserThatIsAlreadyDeactivated()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $user = factory(User::class)->create([
            'active' => false,
        ]);

        $response = $this->delete(route('users.deactivate', ['user' => $user->id]));
        $response->assertRedirect();
    }

    /**
     * Assert that a user cannot deactivate a user with an invalid ID.
     */
    public function testDeleteUserWithInvalidId()
    {
        $authenticatedUser = factory(User::class)->create();
        $this->actingAs($authenticatedUser);

        $response = $this->delete(route('users.deactivate', ['user' => 0]));
        $response->assertRedirect();
    }
}
