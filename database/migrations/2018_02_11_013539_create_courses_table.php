<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCoursesTable
 */
class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('crn')->unique();
            $table->string('subject_code');
            $table->string('course_number');
            $table->string('section');
            $table->string('title');
            $table->date('start_date')->comment('Stored in +0000 time zone, or UTC');
            $table->date('end_date')->comment('Stored in +0000 time zone, or UTC');
            $table->time('start_time')->comment('Stored in +0000 time zone, or UTC');
            $table->time('end_time')->comment('Stored in +0000 time zone, or UTC');
            $table->string('timezone')->default('UTC');
            $table->boolean('monday')->default(false);
            $table->boolean('tuesday')->default(false);
            $table->boolean('wednesday')->default(false);
            $table->boolean('thursday')->default(false);
            $table->boolean('friday')->default(false);
            $table->unsignedInteger('course_term_id');
            $table->foreign('course_term_id')->references('id')->on('course_terms');
            $table->timestamps();
            $table->unsignedInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->unsignedInteger('updated_by');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
