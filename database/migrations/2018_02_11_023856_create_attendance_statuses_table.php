<?php

use App\AttendanceStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateAttendanceStatusesTable
 */
class CreateAttendanceStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->unsignedInteger('display_order')->unique();
            $table->timestamps();
        });

        factory(AttendanceStatus::class)->create([
            'name' => 'Absent',
            'display_order' => 1,
        ]);

        factory(AttendanceStatus::class)->create([
            'name' => 'Tardy',
            'display_order' => 2,
        ]);

        factory(AttendanceStatus::class)->create([
            'name' => 'Present',
            'display_order' => 3,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_statuses');
    }
}
