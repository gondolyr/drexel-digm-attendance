<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    $user = factory(App\User::class)->create();

    $startDate = $faker->dateTimeBetween();
    $endDate = $faker->dateTimeBetween($startDate, $startDate->format('Y-m-d H:i:s') . ' +3 months');

    $startTime = $faker->dateTimeBetween();
    $endTime = $faker->dateTimeBetween($startTime, $startTime->format('Y-m-d H:i:s') . ' +50 minutes');

    return [
        'crn' => $faker->unique()->randomNumber(8),
        'subject_code' => $faker->word,
        'course_number' => (string)$faker->randomNumber(3),
        'section' => (string)$faker->randomNumber(3),
        'title' => $faker->sentence(),
        'start_date' => $startDate->format('Y-m-d'),
        'end_date' => $endDate->format('Y-m-d'),
        'start_time' => $startTime->format('H:i'),
        'end_time' => $endTime->format('H:i'),
        'timezone' => $faker->timezone,
        'monday' => $faker->boolean,
        'tuesday' => $faker->boolean,
        'wednesday' => $faker->boolean,
        'thursday' => $faker->boolean,
        'friday' => $faker->boolean,
        'course_term_id' => function () {
            return factory(App\CourseTerm::class)->create()->id;
        },
        'created_by' => $user->id,
        'updated_by' => $user->id,
    ];
});
