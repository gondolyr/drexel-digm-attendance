@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">Users</div>

        <div>
            @include('layouts.flash')

            <table class="table">
                <thead>
                    <tr class="table-header-row">
                        <th class="table-header-cell dash-user-first-name">First Name</th>
                        <th class="table-header-cell dash-user-last-name">Last Name</th>
                        <th class="table-header-cell dash-user-email">Email Address</th>
                        <th class="table-header-cell dash-user-username">Username</th>
                        <th class="table-header-cell dash-user-role">Roles</th>
                        <th class="table-header-cell dash-user-active">Active</th>
                        <th class="table-header-cell dash-actions">
                            <a href="{{ route('users.create') }}" class="btn btn-primary btn-link">Add User</a>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @if ($users->isNotEmpty())
                        @foreach ($users as $user)
                            <tr class="table-content-row">
                                <td class="table-content-cell dash-user-first-name">{{ $user->first_name }}</td>

                                <td class="table-content-cell dash-user-last-name">{{ $user->last_name }}</td>

                                <td class="table-content-cell dash-user-email">{{ $user->email }}</td>

                                <td class="table-content-cell dash-user-username">{{ $user->username }}</td>

                                <td class="table-content-cell dash-user-role">
                                    <ul>
                                        @foreach ($user->roles as $role)
                                            <li>{{ $role->name }}</li>
                                        @endforeach
                                    </ul>
                                </td>

                                <td class="table-content-cell dash-user-active">{{ $user->is_active_readable }}</td>

                                <td class="table-content-cell dash-actions">
                                    @if ($user->active)
                                        <form class="inline-form" method="POST" action="{{ route('users.deactivate', ['user' => $user->id]) }}">
                                            @method('DELETE')
                                            @csrf

                                            <button type="submit" class="btn btn-red btn-submit" onclick="window.confirm('Are you sure you want to remove {{ $user->full_name }}?');">Deactivate</button>
                                        </form>
                                    @else
                                        <form class="inline-form" method="POST" action="{{ route('users.reactivate', ['user' => $user->id]) }}">
                                            @csrf

                                            <button type="submit" class="btn btn-red btn-submit" onclick="window.confirm('Are you sure you want to reactivate {{ $user->full_name }}?');">Activate</button>
                                        </form>
                                    @endif

                                    <a href="{{ route('users.show', ['user' => $user->id]) }}" class="btn btn-primary btn-link">Details</a>

                                    <a href="{{ route('users.edit', ['user' => $user->id]) }}" class="btn btn-primary btn-link">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr class="table-content-row">
                            <td class="table-content-cell" colspan="5">There are no other users registered.</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
