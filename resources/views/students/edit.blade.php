@extends('layouts.app')

@section('content')
@php
    $isNewStudent = !isset($student);
@endphp

<div class="container">
    <div class="panel-heading">
        @if ($isNewStudent)
            Create Student
        @else
            Edit Student
        @endif
    </div>

    <div>
        @include('layouts.flash')

        <form class="standard-form"
              method="POST"
              action="{{ route(
                'students.' . ($isNewStudent ? 'store' : 'update'),
                $isNewStudent ? [] : ['student' => $student->id]
              ) }}">
            @csrf
            @if (!$isNewStudent)
                @method('PATCH')
            @endif

            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first-name" class="control-label required">First Name</label>

                <div class="form-field">
                    <input type="text"
                           id="first-name"
                           class="form-control"
                           name="first_name"
                           value="{{ old('first_name', $isNewStudent ? '' : $student->first_name) }}"
                           placeholder="John"
                           required
                           autofocus>

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="last-name" class="control-label required">Last Name</label>

                <div class="form-field">
                    <input type="text"
                           id="last-name"
                           class="form-control"
                           name="last_name"
                           value="{{ old('last_name', $isNewStudent ? '' : $student->last_name) }}"
                           placeholder="Smith"
                           required
                           autofocus>

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('university_id') ? ' has-error' : '' }}">
                <label for="university-id" class="control-label required">Student ID</label>

                <div class="form-field">
                    <input type="text"
                           id="university-id"
                           class="form-control"
                           name="university_id"
                           value="{{ old('university_id', $isNewStudent ? '' : $student->university_id) }}"
                           placeholder="12345678"
                           required>

                    @if ($errors->has('university_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('university_id') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label required">Student Email</label>

                <div class="form-field">
                    <input type="email"
                           id="email"
                           class="form-control"
                           name="email"
                           value="{{ old('email', $isNewStudent ? '' : $student->email) }}"
                           placeholder="example@example.com"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-submit-buttons">
                <a class="btn btn-primary btn-link btn-submit" href="{{ route('students.index') }}">
                    Cancel
                </a>

                <button type="submit" class="btn btn-primary btn-submit">
                    @if ($isNewStudent)
                        Create
                    @else
                        Update
                    @endif
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
