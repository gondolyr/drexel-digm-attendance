@extends('layouts.app')

@section('content')
@php
    $isNewCourse = !isset($course);
@endphp

<div class="container">
    <div class="panel-heading">
        @if ($isNewCourse)
            Create Class
        @else
            Edit Class
        @endif
    </div>

    <div>
        @include('layouts.flash')

        <form class="standard-form"
              method="POST"
              action="{{ route(
                  'courses.' . ($isNewCourse ? 'store' : 'update'),
                  $isNewCourse ? [] : ['course' => $course->id]
              ) }}">
            @csrf
            @if (!$isNewCourse)
                @method('PATCH')
            @endif

            <fieldset class="course-details-group">
                <div class="form-group{{ $errors->has('crn') ? ' has-error' : '' }}">
                    <label for="crn" class="control-label required">CRN</label>

                    <div class="form-field">
                        <input type="text"
                               id="crn"
                               class="form-control"
                               name="crn"
                               value="{{ old('crn', $isNewCourse ? '' : $course->crn) }}"
                               placeholder="12345"
                               required
                               autofocus>

                        @if ($errors->has('crn'))
                            <span class="help-block">
                                <strong>{{ $errors->first('crn') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('subject_code') ? ' has-error' : '' }}">
                    <label for="subject-code" class="control-label required">Subject Code</label>

                    <div class="form-field">
                        <input type="text"
                               id="subject-code"
                               class="form-control"
                               name="subject_code"
                               value="{{ old('subject_code', $isNewCourse ? '' : $course->subject_code) }}"
                               placeholder="DIGM"
                               required
                               autofocus>

                        @if ($errors->has('subject_code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('subject_code') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('course_number') ? ' has-error' : '' }}">
                    <label for="course-number" class="control-label required">Course Number</label>

                    <div class="form-field">
                        <input type="text"
                               id="course-number"
                               class="form-control"
                               name="course_number"
                               value="{{ old('course_number', $isNewCourse ? '' : $course->course_number) }}"
                               placeholder="101"
                               required>

                        @if ($errors->has('course_number'))
                            <span class="help-block">
                                <strong>{{ $errors->first('course_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('section') ? ' has-error' : '' }}">
                    <label for="section" class="control-label required">Section</label>

                    <div class="form-field">
                        <input type="text"
                               id="section"
                               class="form-control"
                               name="section"
                               value="{{ old('section', $isNewCourse ? '' : $course->section) }}"
                               placeholder="001"
                               required>

                        @if ($errors->has('section'))
                            <span class="help-block">
                                <strong>{{ $errors->first('section') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="control-label required">Class Title</label>

                    <div class="form-field">
                        <input type="text"
                               id="title"
                               class="form-control"
                               name="title"
                               value="{{ old('title', $isNewCourse ? '' : $course->title) }}"
                               placeholder="Web Authoring I"
                               required>

                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <fieldset class="course-time-group">
                <div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
                    <label for="start-date" class="control-label required">Start Date</label>

                    <div class="form-field">
                        <input type="date"
                               id="start-date"
                               class="form-control"
                               name="start_date"
                               value="{{ old('start_date', $isNewCourse ? '' : $course->start_date) }}"
                               required>

                        @if ($errors->has('start_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                    <label for="end-date" class="control-label required">End Date</label>

                    <div class="form-field">
                        <input type="date"
                               id="end-date"
                               class="form-control"
                               name="end_date"
                               value="{{ old('end_date', $isNewCourse ? '' : $course->end_date) }}"
                               placeholder="001"
                               required>

                        @if ($errors->has('end_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
                    <label for="start-time" class="control-label required">Start Time</label>

                    <div class="form-field">
                        <input type="time"
                               id="start-time"
                               class="form-control"
                               name="start_time"
                               value="{{ $isNewCourse ? '' : $course->start_time_24 }}"
                               min="09:00"
                               max="18:30"
                               required>

                        @if ($errors->has('start_time'))
                            <span class="help-block">
                                <strong>{{ $errors->first('start_time') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
                    <label for="end-time" class="control-label required">End Time</label>

                    <div class="form-field">
                        <input type="time"
                               id="end-time"
                               class="form-control"
                               name="end_time"
                               value="{{ $isNewCourse ? '' : $course->end_time_24 }}"
                               min="09:50"
                               max="21:50"
                               required>

                        @if ($errors->has('section'))
                            <span class="help-block">
                                <strong>{{ $errors->first('section') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                {{-- Currently only supporting one value so no need to display this to the user. --}}
                <input type="hidden" name="timezone" value="America/New_York">

                <div class="form-group{{ $errors->has('course_term_id') ? ' has-error' : '' }}">
                    <label for="course-term-id" class="control-label required">Term</label>

                    <div class="form-field">
                        <select id="course-term-id"
                                class="form-control dropdown"
                                name="course_term_id">
                            @foreach ($courseTerms as $term)
                                <option value="{{ $term->id }}"
                                        @if (old('course_term_id') == $term->id ||
                                            (!$isNewCourse && $term->id == $course->course_term_id)) selected @endif>
                                    {{ $term->name }}
                                </option>
                            @endforeach
                        </select>

                        @if ($errors->has('course_term_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('course_term_id') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>

            <div class="form-group course-meeting-days-container single-field{{ ($errors->has('monday') ||
                                                                                 $errors->has('tuesday') ||
                                                                                 $errors->has('wednesday') ||
                                                                                 $errors->has('thursday') ||
                                                                                 $errors->has('friday')) ? ' has-error' : '' }}">
                <fieldset class="course-meeting-days">
                    <legend class="control-label required" title="At least one day must be selected">Meeting Days</legend>

                    <div class="form-field checkbox-container">
                        <input type="checkbox"
                               id="monday"
                               class="form-control checkbox"
                               name="monday"
                               value="1"
                               @if (old('monday', $isNewCourse ? false : $course->monday)) checked @endif>
                        <label for="monday">Monday</label>
                    </div>

                    <div class="form-field checkbox-container">
                        <input type="checkbox"
                               id="tuesday"
                               class="form-control checkbox"
                               name="tuesday"
                               value="1"
                               @if (old('tuesday', $isNewCourse ? false : $course->tuesday)) checked @endif>
                        <label for="tuesday">Tuesday</label>
                    </div>

                    <div class="form-field checkbox-container">
                        <input type="checkbox"
                               id="wednesday"
                               class="form-control checkbox"
                               name="wednesday"
                               value="1"
                               @if (old('wednesday', $isNewCourse ? false : $course->wednesday)) checked @endif>
                        <label for="wednesday">Wednesday</label>
                    </div>

                    <div class="form-field checkbox-container">
                        <input type="checkbox"
                               id="thursday"
                               class="form-control checkbox"
                               name="thursday"
                               value="1"
                               @if (old('thursday', $isNewCourse ? false : $course->thursday)) checked @endif>
                        <label for="thursday">Thursday</label>
                    </div>

                    <div class="form-field checkbox-container">
                        <input type="checkbox"
                               id="friday"
                               class="form-control checkbox"
                               name="friday"
                               value="1"
                               @if (old('friday', $isNewCourse ? false : $course->friday)) checked @endif>
                        <label for="friday">Friday</label>
                    </div>
                </fieldset>

                <ul class="errors">
                    @if ($errors->has('monday'))
                        <li class="help-block">
                            <strong>{{ $errors->first('monday') }}</strong>
                        </li>
                    @endif

                    @if ($errors->has('tuesday'))
                        <li class="help-block">
                            <strong>{{ $errors->first('tuesday') }}</strong>
                        </li>
                    @endif

                    @if ($errors->has('wednesday'))
                        <li class="help-block">
                            <strong>{{ $errors->first('wednesday') }}</strong>
                        </li>
                    @endif

                    @if ($errors->has('thursday'))
                        <li class="help-block">
                            <strong>{{ $errors->first('thursday') }}</strong>
                        </li>
                    @endif

                    @if ($errors->has('friday'))
                        <li class="help-block">
                            <strong>{{ $errors->first('friday') }}</strong>
                        </li>
                    @endif
                </ul>
            </div>

            @if (auth()->user()->is_admin && $instructors->isNotEmpty())
                <div class="form-group single-field">
                    @if ($errors->has('instructors'))
                        <span class="errors help-block">
                            <strong>{{ $errors->first('instructors') }}</strong>
                        </span>
                    @endif

                    <table class="table">
                        <caption class="table-title control-label required{{ $errors->has('instructors') ? ' has-error' : '' }}"
                                 title="At least one instructor must be selected">Instructors</caption>

                        <thead>
                            <tr class="table-header-row">
                                <th class="table-header-cell dash-student-first-name">First Name</th>
                                <th class="table-header-cell dash-student-last-name">Last Name</th>
                                <th class="table-header-cell dash-actions">Add to class</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr class="table-content-row">
                                <td class="table-content-cell" colspan="2">
                                    <label for="instructor-self">Add myself to class</label>
                                </td>

                                <td class="table-content-cell form-field checkbox-container">
                                    <input type="checkbox"
                                           id="instructor-self"
                                           class="table-input-centered form-control checkbox"
                                           name="instructors[]"
                                           value="{{ auth()->user()->id }}"
                                           {{ in_array(auth()->user()->id, old('instructors', [])) ||
                                              ($isNewCourse || !is_null(auth()->user()->courses()->where('id', $course->id))) ? 'checked' : '' }}>
                                </td>
                            </tr>

                            @foreach ($instructors as $instructor)
                                <tr class="table-content-row">
                                    <td class="table-content-cell">
                                        <label for="instructor-{{ $instructor->id }}">{{ $instructor->first_name }}</label>
                                    </td>

                                    <td class="table-content-cell">
                                        <label for="instructor-{{ $instructor->id }}">{{ $instructor->last_name }}</label>
                                    </td>

                                    <td class="table-content-cell form-field checkbox-container">
                                        <input type="checkbox"
                                               id="instructor-{{ $instructor->id }}"
                                               class="table-input-centered form-control checkbox"
                                               name="instructors[]"
                                               value="{{ $instructor->id }}"
                                               {{ in_array($instructor->id, old('instructors', [])) ||
                                                  (!$isNewCourse && is_null(old('instructors')) && $instructor->courses->contains('id', $course->id)) ? 'checked' : '' }}>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

            @if ($students->isNotEmpty())
                <div class="form-group single-field">
                    @if ($errors->has('students'))
                        <span class="errors help-block">
                            <strong>{{ $errors->first('students') }}</strong>
                        </span>
                    @endif

                    <table class="table">
                        <caption class="table-title control-label required{{ $errors->has('students') ? ' has-error' : '' }}"
                                 title="At least one student must be selected">Students</caption>

                        <thead>
                            <tr class="table-header-row">
                                <th class="table-header-cell">First Name</th>
                                <th class="table-header-cell">Last Name</th>
                                <th class="table-header-cell dash-actions">Add to class</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($students as $student)
                                <tr class="table-content-row">
                                    <td class="table-content-cell">
                                        <label for="student-{{ $student->id }}">{{ $student->first_name }}</label>
                                    </td>

                                    <td class="table-content-cell">
                                        <label for="student-{{ $student->id }}">{{ $student->last_name }}</label>
                                    </td>

                                    <td class="table-content-cell form-field checkbox-container">
                                        <input type="checkbox"
                                               id="student-{{ $student->id }}"
                                               class="table-input-centered form-control checkbox"
                                               name="students[]"
                                               value="{{ $student->id }}"
                                               {{ in_array($student->id, old('students', [])) ||
                                                  (!$isNewCourse && is_null(old('students')) && $student->courses->contains('id', $course->id)) ? 'checked' : '' }}>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

            <div class="form-submit-buttons">
                <a class="btn btn-primary btn-link btn-submit" href="{{ url()->previous() }}">
                    Cancel
                </a>

                <button type="submit" class="btn btn-primary btn-submit">
                    @if ($isNewCourse)
                        Create
                    @else
                        Update
                    @endif
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
