@extends('layouts.app')

@section('content')
<div class="container">
    <div class="panel-heading">
        <p>{{ $course->course_code }}</p>
        <p>{{ $course->title }}</p>
    </div>

    @include('layouts.flash')

    <div class="course-grid">
        <table class="table course-details start">
            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">CRN:</td>
                <td class="table-content-cell course-detail-cell"> {{ $course->crn }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Start Date:</td>
                <td class="table-content-cell course-detail-cell">{{ $course->start_date }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">End Date:</td>
                <td class="table-content-cell course-detail-cell">{{ $course->end_date }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Start Time:</td>
                <td class="table-content-cell course-detail-cell">{{ $course->start_time_12 }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">End Time:</td>
                <td class="table-content-cell course-detail-cell">{{ $course->end_time_12 }}</td>
            </tr>

            <tr class="table-content-row">
                <td class="table-content-cell course-detail-cell course-detail-label">Meeting Days:</td>
                <td class="table-content-cell course-detail-cell">
                    <ul>
                        @if ($course->monday)
                            <li>Monday</li>
                        @endif
                        @if ($course->tuesday)
                            <li>Tuesday</li>
                        @endif
                        @if ($course->wednesday)
                            <li>Wednesday</li>
                        @endif
                        @if ($course->thursday)
                            <li>Thursday</li>
                        @endif
                        @if ($course->friday)
                            <li>Friday</li>
                        @endif
                    </ul>
                </td>
            </tr>
        </table>

        <ul class="end">
            <li class="course-detail-label">Instructors:</li>

            @foreach ($course->instructors as $instructor)
                <li>
                    <a href="{{ route('users.show', ['user' => $instructor->id]) }}">{{ $instructor->full_name }}</a>
                </li>
            @endforeach
        </ul>

        <div class="start">
            <a href="{{ route('courses.edit', ['course' => $course->id]) }}" class="btn btn-primary btn-link">Edit Class</a>
        </div>
    </div>

    <table class="table dash-attendance">
        <caption class="table-title">Students</caption>

        <thead>
            <tr class="table-header-row">
                <th class="table-header-cell dash-attendance-first-name">First Name</th>
                <th class="table-header-cell dash-attendance-last-name">Last Name</th>
                <th class="table-header-cell dash-attendance-absences">Absences</th>
                <th class="table-header-cell dash-attendance-tardies">Tardies</th>
                <th class="table-header-cell dash-attendance-on-time">On Time</th>
                <th class="table-header-cell dash-attendance-excused">Excused</th>
                <th class="table-header-cell dash-actions"></th>
            </tr>
        </thead>

        <tbody>
            @if ($course->students->isNotEmpty())
                @foreach ($course->students as $student)
                    <tr class="table-content-row">
                        <td class="table-content-cell dash-attendance-first-name">{{ $student->first_name }}</td>

                        <td class="table-content-cell dash-attendance-last-name">{{ $student->last_name }}</td>

                        <td class="table-content-cell table-number dash-attendance-absences">
                            {{ is_null($student->courseAttendances)
                                ? 0
                                : $student->courseAttendances->where('attendance_status_id', $absentStatusId)->count() }}
                        </td>

                        <td class="table-content-cell table-number dash-attendance-tardies">
                            {{ is_null($student->courseAttendances)
                                ? 0
                                : $student->courseAttendances->where('attendance_status_id', $tardyStatusId)->count() }}
                        </td>

                        <td class="table-content-cell table-number dash-attendance-on-time">
                            {{ is_null($student->courseAttendances)
                                ? 0
                                : $student->courseAttendances->where('attendance_status_id', $presentStatusId)->count() }}
                        </td>

                        <td class="table-content-cell table-number dash-attendance-excused">
                            {{ is_null($student->courseAttendances)
                                ? 0
                                : $student->courseAttendances->where('excused', true)->count() }}
                        </td>

                        <td class="table-content-cell dash-actions">
                            <a href="{{ route('attendances.students.edit', ['course' => $course->id, 'student' => $student->id]) }}" class="btn btn-primary btn-link">Edit Attendance</a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr class="table-content-row">
                    <td class="table-content-cell" colspan="7">There are no students registered with this class.</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
@endsection
