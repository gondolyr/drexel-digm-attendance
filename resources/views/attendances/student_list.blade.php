@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="panel-heading">
            <p>Attendance for {{ $student->full_name }}</p>
            <p>{{ $course->course_code }}</p>
        </div>

        <div>
            @include('layouts.flash')

            <form id="attendance-form"
                  class="attendance-student-update-form"
                  method="POST"
                  action="{{ route('attendances.students.update', ['course' => $course->id, 'student' => $student->id]) }}">
                @method('PATCH')
                @csrf

                <table class="table">
                    <thead>
                        <tr class="table-header-row">
                            <th class="table-header-cell dash-attendance-student-date">Date</th>
                            <th class="table-header-cell dash-attendance-student-day-of-week">Day of Week</th>
                            <th class="table-header-cell dash-actions">Status</th>
                            <th class="table-header-cell dash-attendance-student-note">Note</th>
                        </tr>
                    </thead>

                    <tbody>
                        @if ($student->courseAttendances->isNotEmpty())
                            @foreach ($student->courseAttendances as $attendance)
                                <tr class="table-content-row">
                                    <td class="table-content-cell dash-attendance-student-date">{{ $attendance->getCreatedAtDateTime12($course->timezone) }}</td>

                                    <td class="table-content-cell dash-attendance-student-day-of-week">{{ $attendance->getCreatedAtDayOfWeek($course->timezone) }}</td>

                                    <td class="table-content-cell">
                                        <div class="dash-actions">
                                            @foreach ($attendanceStatuses as $status)
                                                <div class="radio-container">
                                                    <input type="radio"
                                                           id="attendance-status-{{ $status->name }}-{{ $attendance->id }}"
                                                           class="radio radio-option"
                                                           name="attendances[{{ $attendance->id }}][status]"
                                                           value="{{ $status->id }}"
                                                           {{ (old('attendances[' . $attendance->id . '][status]') == $status->id) ||
                                                              ($status->id == $attendance->attendance_status_id)
                                                                ? 'checked'
                                                                : '' }}>

                                                    <label for="attendance-status-{{ $status->name }}-{{ $attendance->id }}">{{ $status->name }}</label>

                                                    @if ($errors->has('attendances[' . $attendance->id . '][status]'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('attendances[' . $attendance->id . '][status]') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            @endforeach

                                            <div class="checkbox-with-label-container">
                                                <input type="checkbox"
                                                       id="attendance-excused-{{ $attendance->id }}"
                                                       class="checkbox"
                                                       name="attendances[{{ $attendance->id }}][excused]"
                                                       value="1"
                                                       {{ old('attendances[' . $attendance->id . '][excused]') == 1 ||
                                                          ($attendance->excused == 1)
                                                            ? 'checked'
                                                            : '' }}>

                                                <label for="attendance-excused-{{ $attendance->id }}">Excused</label>

                                                @if ($errors->has('attendances[' . $attendance->id . '][excused]'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('attendances[' . $attendance->id . '][excused]') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </td>

                                    <td class="table-content-cell dash-attendance-student-note">
                                        <textarea id="attendance-note-{{ $attendance->id }}"
                                                  class="textarea form-control"
                                                  name="attendances[{{ $attendance->id }}][note]"
                                                  placeholder="What is the reason for the status?"
                                                  cols="30"
                                                  rows="2">{{ old('attendances[' . $attendance->id . '][note]', $attendance->note) }}</textarea>

                                        @if ($errors->has('attendances[' . $attendance->id . '][note]'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('attendances[' . $attendance->id . '][note]') }}</strong>
                                            </span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="table-content-row">
                                <td class="table-content-cell" colspan="3">There are no attendance records.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </form>

            <div class="form-submit-buttons">
                <a class="btn btn-primary btn-link btn-submit" href="{{ route('courses.show', ['course' => $course->id]) }}">
                    Cancel
                </a>

                <button type="submit" form="attendance-form" class="btn btn-primary btn-submit">Save</button>
            </div>
        </div>
    </div>
@endsection
