@extends('layouts.app')

@section('content')
<div class="container">
    <div class="form-heading-container">
        <img src="{{ asset('img/westphal-logo.png') }}" alt="Westphal Logo" class="guest-logo">
        <div class="panel-heading">Register Administrator User</div>
    </div>

    <div>
        <form class="guest-form shadow" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                <label for="first-name" class="control-label required">First Name</label>

                <div class="form-field">
                    <input type="text"
                           id="first-name"
                           class="form-control guest-input"
                           name="first_name"
                           value="{{ old('first_name') }}"
                           placeholder="John"
                           required
                           autofocus>

                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                <label for="last-name" class="control-label required">Last Name</label>

                <div class="form-field">
                    <input type="text"
                           id="last-name"
                           class="form-control guest-input"
                           name="last_name"
                           value="{{ old('last_name') }}"
                           placeholder="Smith"
                           required
                           autofocus>

                    @if ($errors->has('last_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label required">E-Mail Address</label>

                <div class="form-field">
                    <input type="email"
                           id="email"
                           class="form-control guest-input"
                            name="email"
                            value="{{ old('email') }}"
                           placeholder="example@example.com"
                            required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="control-label required">Username</label>

                <div class="form-field">
                    <input type="text"
                           id="username"
                           class="form-control guest-input"
                           name="username"
                           value="{{ old('username') }}"
                           placeholder="abc123"
                           required>

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label required">Password</label>

                <div class="form-field">
                    <input type="password"
                           id="password"
                           class="form-control guest-input"
                           name="password"
                           required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="control-label required">Confirm Password</label>

                <div class="form-field">
                    <input type="password"
                           id="password-confirm"
                           class="form-control guest-input"
                           name="password_confirmation"
                           required>
                </div>
            </div>

            <div class="form-submit-buttons">
                <button type="submit" class="btn btn-primary btn-submit">
                    Register
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
